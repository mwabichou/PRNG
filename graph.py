import matplotlib.pyplot as plt
import numpy as np


def plot_histogram(values, bins):
    hist, edges = np.histogram(values, bins=bins, range=(0, max(values) * 3))
    plt.bar(edges[:-1], hist, width=edges[1] - edges[0])
    plt.xlim(0, max(values) * 3)
    plt.xlabel("Value")
    plt.ylabel("Frequency")
    plt.title("Histogram of Negative Exponential Values")
    plt.show()


def plot_histogram_with_mean(values):

    mean = np.mean(values)
    plt.axhline(mean, color='red', linestyle='--', linewidth=2, label='Mean')
    plt.legend()
    plt.show()


def graphique_probabilite1(x, y):
    plt.rcParams["figure.figsize"] = (25, 10)
    plt.bar(x, y,
            color="#62bfed", ec="black")
    # plt.axhline(y=14, color="#62bfed", linestyle='--')
    plt.title("distribution cumulative", fontweight="bold",
              backgroundcolor="#0abdca", fontsize="30", pad="20", color="white")
    plt.grid(True, alpha=0.7)
    plt.legend()
    plt.savefig("img/graphique4.png", dpi=300, transparent=True)
    plt.show()


def graphique_probabilite2(x, y):
    plt.rcParams["figure.figsize"] = (25, 10)
    # plt.bar(x, y,
    #         label="nombre de tas maximum", color="#62bfed", ec="black")
    plt.plot(x, y, color='#62bfed', linewidth=2, markersize=10)
    # plt.axvline(x=-0.2, color="#fbcaef",
    #             label="Moyenne", linestyle='--')
    plt.title("Distribution exponentielle ", fontweight="bold",
              backgroundcolor="#0abdca", fontsize="30", pad="20", color="white")
    # plt.xlabel("Nombre de tas max", fontweight="bold")
    # plt.ylabel("probabilté de gagner", fontweight="bold")
    plt.grid(True, alpha=0.1)
    plt.legend()
    plt.savefig("img/graphique3.png", dpi=300, transparent=True)
    plt.show()


    # plt.show()
if __name__ == '__main__':
    y = [9597, 8707, 7915, 6983, 6432, 5736, 5236, 4646, 4273, 3906,
         3573, 3121, 2874, 2495, 2234, 2124, 1936, 1762, 1550, 1423]
    # y = [67, 553, 4109, 22336, 97161, 331015, 881238, 1837760, 2999903, 3829112,
    #      3825531, 2998071, 1837335, 880915, 331317, 96739, 22070, 4077, 604, 81]
    # y = [33, 253, 2064, 11191, 48480, 165841, 440487, 918875, 1499504, 1915796,
    #      1913058, 1498152, 918702, 440208, 165329, 48442, 11143, 2063, 337, 39]
    x = list(range(20))
    # x = list(range(0, 241))
    # y = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 2, 1, 1, 5, 2, 3, 4, 5, 7, 7, 9, 8, 6, 14,
    #      16, 15, 20, 17, 15, 34, 23, 30, 36, 22, 30, 30, 35, 35, 33, 35, 42, 36, 29, 30, 40, 31, 33, 28, 32, 22, 22, 22, 19, 16, 23, 9, 9, 14, 9, 8, 6, 4, 3, 1, 4, 1, 2, 2, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    # # y = [8, 59, 422, 2223, 9870, 33259, 88082, 183659, 299356, 383417,
    # #      382670, 299947, 183405, 88007, 33201, 9676, 2255, 413, 60, 9]
    #x = np.arange(-5, 5, 0.5)
    # # Tracé de la courbe
    # plt.plot(x, y, color='red', linewidth=2, markersize=10)

    # # Ajout de la moyenne
    # mean = np.mean(-0.2)
    # #plt.axhline(mean, color='blue', linestyle='--', label='Moyenne')
    # plt.axvline(mean, color='blue', linestyle='--', label='Moyenne')
    # # Ajout d'un titre et des labels pour les axes
    # plt.title(
    #     'Simulation of the Inverse function of the uniform and negative exponential law')
    # # plt.xlabel('Abscisses')
    # # plt.ylabel('Ordonnées')

    # # Ajout de la légende
    # plt.legend()

    # # Affichage de la figure
    # plt.show()
    y = [100, 500, 1100, 1500, 1600, 1800]
    x = list(range(1, 7))
    graphique_probabilite1(x, y)
    # graphique_probabilite2(x, y)
