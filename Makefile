SRC	=	src/main.c \
		src/mt.c \
		src/box_muller.c \
		src/dice.c \
		src/empirical.c \
		src/neg_exp.c \
		src/uniform.c \
		src/rejection.c \

OBJ	=	$(SRC:.c=.o)

NAME	=	tp2

all:	$(NAME)

$(NAME):
	gcc $(SRC) -Wall -v -I src/include -o $(NAME) -g -lm

clean:
	rm -f $(OBJ)
	rm -rf *.dSYM

fclean:	clean
	rm -f $(NAME)

re:	fclean all
run:
	gcc $(SRC) -Wall -v -I src/include -o $(NAME) -g -lm
	./$(NAME)

.PHONY:	all clean fclean re