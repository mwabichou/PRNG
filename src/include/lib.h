#ifndef lib
#define lib
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#define SIZE_BUFF 512
#define N 624
#define M 397
#define MATRIX_A 0x9908b0dfUL   /* constant vector a */
#define UPPER_MASK 0x80000000UL /* most significant w-r bits */
#define LOWER_MASK 0x7fffffffUL /* least significant r bits */
static unsigned long mt[N];     /* the array for the state vector  */
static int mti = N + 1;         /* mti==N+1 means mt[N] is not initialized */

void init_genrand(unsigned long s);
void init_by_array(unsigned long init_key[], int key_length);
unsigned long genrand_int32(void);
long genrand_int31(void);
double genrand_real1(void);
double genrand_real2(void);
double genrand_real3(void);
double genrand_res53(void);
void uniform(double a, double b);
void empirical_distributions(int n);
void generic_empirical_distributions(int taille, double *tab, int n);
double negExp(double mean);
void negExp_drawing(double mean, int n);
void biased_dice(int taille, double mean, int n);
double rejection(double MinX, double MaxX, double MinY, double MaxY, double (*f)(double));
double f(double x);
double dice_experiment(int taille, int n);
double *dice_experiment_drawing(int n);
void box_muller(double *x1, double *x2);
void test_box_muller(int n, double mean, double sigma);
#endif /*lib*/