#include "lib.h"

/* -------------------------------------------------------------------------- */
/* Nom de la fonction : biased_dice                                           */
/*                                                                            */
/* Entrées : Un entier "taille" représentant la taille du tableau "bins_tab"  */
/*           Un double "mean" représentant la moyenne de la loi exponentielle */
/*           Un entier "n" représentant le nombre de tirages à effectuer      */
/*                                                                            */
/* Sorties : Affiche les résultats des tirages sous forme de histogramme et la moyenne des valeurs obtenues.*/
/*                                                                            */
/* Valeur de retour : Aucune valeur de retour.                                */
/* -------------------------------------------------------------------------- */
void biased_dice(int taille, double mean, int n)
{
    int *bins_tab = calloc(taille, sizeof(int));
    double r, moyenne = 0;
    for (int i = 0; i < n; i++)
    {
        r = negExp(mean);
        r >= taille ? bins_tab[taille - 1]++ : bins_tab[(int)r]++;
    }
    printf("Apres %d tirages : \n", n);
    for (int i = 0; i < taille; i++)
    {
        i != taille - 1 ? printf("[%d-%d]: %d \n", i, i + 1, bins_tab[i]) : printf("[%d-...]: %d \n", i, bins_tab[i]);
        moyenne += bins_tab[i] * (i + 1);
    }
    printf("Moyenne: %f \n", moyenne / n);
    free(bins_tab);
}
/* -------------------------------------------------------------------------- */
/* Nom de la fonction : dice_experiment                                       */
/*                                                                            */
/* Entrées : Un entier "taille" représentant le nombre de faces               */
/*           Un entier "n" représentant le nombre de tirages à effectuer      */
/*                                                                            */
/* Sorties : Affiche la moyenne des valeurs obtenues.                         */
/*                                                                            */
/* Valeur de retour : Un double représentant la somme des valeurs obtenues. */
/* -------------------------------------------------------------------------- */
double dice_experiment(int taille, int n)
{
    double sum = 0;
    for (int i = 0; i < n; i++)
    {
        sum += genrand_int31() % taille + 1;
    }
    return sum;
}
/* -------------------------------------------------------------------------- */
/* Nom de la fonction : dice_experiment_drawing                               */
/*                                                                            */
/* Entrées : Un entier "n" représentant le nombre de tirages à effectuer      */
/*                                                                            */
/* Sorties : Affiche la moyenne des valeurs obtenues.                         */
/*                                                                            */
/* Valeur de retour : Un tableau de double représentant les valeurs obtenues. */
/* -------------------------------------------------------------------------- */
double *dice_experiment_drawing(int n)
{
    double r, total = 0, ecart_type = 0, moyenne = 0;
    double *res = calloc(241, sizeof(double));
    for (int i = 0; i < n; i++)
    {
        r = dice_experiment(6, 40);
        res[(int)r]++;
    }
    for (int i = 0; i < 241; i++)
    {
        moyenne += res[i] * i;
        ecart_type += res[i] * i * i;
    }
    printf("\n");
    moyenne /= n;
    ecart_type = sqrt(ecart_type / (n)-moyenne * moyenne);
    printf("Apres %d tirages : \n", n);
    printf("Moyenne: %.2f \n", moyenne);
    printf("Ecart type: %.2f \n", ecart_type);
    return res;
}
