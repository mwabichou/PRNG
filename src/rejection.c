#include "lib.h"
/* -------------------------------------------------------------------- */
/* Nom de la fonction : rejection                                       */
/*                                                                      */
/* Entrées : Deux doubles "MinX" et "MaxX" définissant les bornes de la */
/*           loi uniforme.                                              */
/*           Un pointeur de fonction "f" définissant la fonction de     */
/*           répartition.                                               */
/*                                                                      */
/* Sorties : Aucune sortie.                                             */
/*                                                                      */
/* Valeur de retour : Un double représentant la valeur tirée.           */
/* -------------------------------------------------------------------- */
double rejection(double MinX, double MaxX, double MinY, double MaxY, double (*f)(double))
{
    double x, y;
    do
    {
        x = MinX + (MaxX - MinX) * genrand_real1();
        y = MinY + (MaxY - MinY) * genrand_real1();
    } while (y > f(x));
    return x;
}

/* -------------------------------------------------------------------- */
/* Nom de la fonction : f                                               */
/*                                                                      */
/* Entrées : Un double "x" définissant la valeur de la fonction.        */
/*                                                                      */
/* Sorties : Aucune sortie.                                             */
/*                                                                      */
/* Valeur de retour : Un double représentant la valeur de la fonction.  */
/* -------------------------------------------------------------------- */
double f(double x)
{
    return 1 / (1 + exp(-x));
}
