#include "lib.h"
/*----------------------------------------------*/
/*                                              */
/* Nom : menu                                   */
/*                                              */
/*----------------------------------------------*/
void menu()
{
    int choix, n, taille;
    double borneInf, borneSup, mean, sigma;
    system("clear");
    do
    {
        printf("\t\t------------------MENU------------------ \n");
        printf("\t\t1. Tirage uniforme \n");
        printf("\t\t2. Tirage empirique \n");
        printf("\t\t3. Tirage avec NegExp \n");
        printf("\t\t4. Tirage de dés biaisés \n");
        printf("\t\t5. Tirage de dés \n");
        printf("\t\t6. Tirage de box muller \n");
        printf("\t\t0. Quitter \n");
        printf("Votre choix: ->");
        scanf("%d", &choix);
        switch (choix)
        {
        case 1:
            printf("Entrez la borne inférieure: ");
            scanf("%lf", &borneInf);
            fflush(stdin);
            printf("Entrez la borne supérieure: ");
            scanf("%lf", &borneSup);
            fflush(stdin);
            uniform(borneInf, borneSup);
            break;
        case 2:
            fflush(stdin);
            printf("Entrez le nombre de tirages: ");
            scanf("%d", &n);
            empirical_distributions(n);
            break;
        case 3:
            fflush(stdin);
            printf("Entrez le nombre de tirages: ");
            scanf("%d", &n);
            printf("Entrez la moyenne: ");
            scanf("%lf", &mean);
            negExp_drawing(mean, n);
            break;
        case 4:
            fflush(stdin);
            printf("Entrez la taille des dés: ");
            scanf("%d", &taille);
            printf("Entrez la moyenne: ");
            scanf("%lf", &mean);
            printf("Entrez le nombre de tirages: ");
            scanf("%d", &n);
            biased_dice(taille, mean, n);
            break;
        case 5:
            fflush(stdin);
            printf("Entrez le nombre de tirages: ");
            scanf("%d", &n);
            dice_experiment_drawing(n);
            break;
        case 6:
            fflush(stdin);
            printf("Entrez le nombre de tirages: ");
            scanf("%d", &n);
            printf("Entrez la moyenne: ");
            scanf("%lf", &mean);
            printf("Entrez sigma: ");
            scanf("%lf", &sigma);
            test_box_muller(n, mean, sigma);
            break;
        case 0:
            printf("Au revoir ! \n");
            break;
        default:
            printf("Choix invalide \n");
            break;
        }
    } while (choix != 0);
}
int main(void)
{
    unsigned long init[4] = {0x123, 0x234, 0x345, 0x456}, length = 4;
    init_by_array(init, length);
    // decommenter pour tester les fonctions
    // // Test uniform function for temperatures between -98°C and 57,7°C (with only one decimal)
    // uniform(-98, 57.7);
    // // Test empirical_distributions1
    // empirical_distributions(1000);
    // empirical_distributions(10000);
    // empirical_distributions(100000);
    // empirical_distributions(1000000);
    // double tab[3] = {350, 450, 200};
    // generic_empirical_distributions(3, tab, 10000);
    // double tabHDL[6] = {100, 400, 600, 400, 100, 200};
    // generic_empirical_distributions(6, tabHDL, 1800);
    // negExp_drawing(10, 1000);
    // negExp_drawing(21, 1000000);
    // biased_dice(21, 10, 1000000);
    // dice_experiment_drawing(1000000);
    // test_box_muller(10000000, 0, 1);

    menu();
    return 0;
}
