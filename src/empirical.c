#include "lib.h"
/* -------------------------------------------------------------------------- */
/* Nom de la fonction : empirical_distributions                               */
/*                                                                            */
/* Entrées : Un entier "n" représentant le nombre de tirages à effectuer      */
/*                                                                            */
/* Sorties : Affiche les résultats des tirages sous forme de histogramme et la moyenne et l'écart type des valeurs obtenues.*/
/*                                                                            */
/* Valeur de retour : Aucune valeur de retour.                                */
/* -------------------------------------------------------------------------- */
void empirical_distributions(int n)
{
    double r, A = 0, B = 0, C = 0;
    for (int i = 0; i < n; i++)
    {
        r = genrand_real1();
        r < 0.35 ? A++ : r < 0.8 ? B++
                                 : C++;
    }
    printf("Apres %d tirages : A: %.2f%%, B: %.2f%%, C: %.2f%% \n", n, (A / n) * 100, (B / n) * 100, (C / n) * 100);
}
/* -------------------------------------------------------------------------- */
/* Nom de la fonction : generic_empirical_distributions                       */
/*                                                                            */
/* Entrées : Un entier "taille" représentant la taille du tableau "tab"       */
/*           Un tableau de double "tab"                                       */
/*           Un entier "n" représentant le nombre de tirages à effectuer      */
/*                                                                            */
/* Sorties : Affiche les résultats des tirages sous forme de histogramme et la moyenne et l'écart type des valeurs obtenues.*/
/*                                                                            */
/* Valeur de retour : Aucune valeur de retour.                                */
/* -------------------------------------------------------------------------- */
void generic_empirical_distributions(int taille, double *tab, int n)
{
    double r, total = 0;
    double *res = calloc(taille, sizeof(double));
    double *cumulative_tab = calloc(taille, sizeof(double));
    int c;

    for (int i = 0; i < taille; i++)
    {
        total += tab[i];
    }
    for (int i = 0; i < taille; i++)
    {
        tab[i] /= total;
    }
    for (int i = 0; i < n; i++)
    {
        r = genrand_real1();
        c = 0;
        while (r > tab[c])
        {
            r -= tab[c];
            c++;
        }
        res[c]++;
    }
    printf("Apres %d tirages : \n", n);

    for (int i = 0; i < taille; i++)
    {
        printf("Class %d: %.2f%% \n", i + 1, res[i] * 100 / n);
    }
    // Probabilité cumulative
    printf("Probabilité cumulative : \n");
    cumulative_tab[0] = tab[0];
    printf("Class %d: %.6f \n", 1, cumulative_tab[0]);
    for (int i = 1; i < taille; i++)
    {
        cumulative_tab[i] = cumulative_tab[i - 1] + tab[i];
        printf("Class %d: %.6f \n", i + 1, cumulative_tab[i]);
    }
    free(res);
}