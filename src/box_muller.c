#include "lib.h"
/* -------------------------------------------------------------------------- */
/* Nom de la fonction : box_muller                                            */
/*                                                                            */
/* Entrées : Deux pointeurs de double "x1" et "x2"                            */
/*                                                                            */
/* Sorties : Affecte à "x1" et "x2" deux valeurs tirées selon une loi normale */
void box_muller(double *x1, double *x2)
{
    double r1, r2;
    r1 = genrand_real3();
    r2 = genrand_real3();
    *x1 = sqrt(-2 * log(r1)) * cos(2 * M_PI * r2);
    *x2 = sqrt(-2 * log(r1)) * sin(2 * M_PI * r2);
}

/* -------------------------------------------------------------------- */
/* Nom de la fonction : test_box_muller                                 */
/*                                                                      */
/* Entrées : Un entier "n" définissant le nombre de tirages à effectuer */
/*           Un double "mean" définissant la moyenne de la loi normale  */
/*           Un double "sigma" définissant l'écart type de la loi normale*/
/*                                                                       */
/* Sorties : Affiche les résultats des tirages sous forme de histogramme et la moyenne et l'écart type des valeurs obtenues.*/
/*                                                                       */
/* Valeur de retour : Aucune valeur de retour.                          */
/* -------------------------------------------------------------------- */
void test_box_muller(int n, double mean, double sigma)
{
    int binsTab[20] = {0}, binVal1, binVal2;
    double x1, x2, moyenne = 0, ecartType = 0;

    for (int i = 0; i < n; i++)
    {
        box_muller(&x1, &x2);
        x1 = x1 * sigma + mean;
        x2 = x2 * sigma + mean;
        (binVal1 = (int)((x1 + 5) / 0.5)) >= 0 && binVal1 < 20 ? binsTab[binVal1]++ : 0;
        (binVal2 = (int)((x2 + 5) / 0.5)) >= 0 && binVal2 < 20 ? binsTab[binVal2]++ : 0;
    }
    printf("Apres %d tirages : \n", n);
    for (int i = 0; i < 20; i++)
    {
        printf("Bin %d: %d\n", i + 1, binsTab[i]);
        moyenne += binsTab[i] * (i * 0.5 - 5);
        ecartType += binsTab[i] * (i * 0.5 - 5) * (i * 0.5 - 5);
    }
    moyenne /= 2 * n;
    ecartType = sqrt(ecartType / (2 * n) - moyenne * moyenne);
    printf("Moyenne: %.2f, Ecart type: %.2f\n", moyenne, ecartType);
}