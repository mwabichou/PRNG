#include "lib.h"
/* -------------------------------------------------------------------- */
/* Nom de la fonction : test_box_muller                                 */
/*                                                                      */
/* Entrées : Un entier "n" définissant le nombre de tirages à effectuer.*/
/*                                                                       */
/* Sorties : Affiche les résultats des tirages sous forme de histogramme et la moyenne et l'écart type des valeurs obtenues.*/
/*                                                                       */
/* Valeur de retour : Aucune valeur de retour.                          */
/* -------------------------------------------------------------------- */

/* -------------------------------------------------------------------- */
/* Nom de la fonction : negExp                                          */
/*                                                                      */
/* Entrées : Un double "mean" définissant la moyenne de la loi          */
/*           exponentielle.                                             */
/*                                                                       */
/* Sorties : Aucune sortie.                                             */
/*                                                                       */
/* Valeur de retour : Un double représentant la valeur tirée.           */
/* -------------------------------------------------------------------- */
double negExp(double mean)
{
    double r = genrand_real2();
    return -mean * log(1 - r);
}
/* -------------------------------------------------------------------- */
/* Nom de la fonction : negExp_drawing                                  */
/*                                                                      */
/* Entrées : Un double "mean" définissant la moyenne de la loi          */
/*           exponentielle.                                             */
/*           Un entier "n" définissant le nombre de tirages à effectuer.*/
/*                                                                       */
/* Sorties : Affiche les résultats des tirages sous forme de histogramme et la moyenne des valeurs obtenues.*/
/*                                                                       */
/* Valeur de retour : Aucune valeur de retour.                          */
/* -------------------------------------------------------------------- */

void negExp_drawing(double mean, int n)
{
    double r, total = 0;
    double *res = calloc(n, sizeof(double));
    for (int i = 0; i < n; i++)
    {
        r = negExp(mean);
        res[i] = r;
        total += r;
    }
    printf("Apres %d tirages : \n Moyenne: %.2f \n", n, total / n);
    free(res);
}