#include "lib.h"
/* -------------------------------------------------------------------- */
/* Nom de la fonction : uniform                                         */
/*                                                                      */
/* Entrées : Deux doubles "a" et "b" définissant les bornes de la loi   */
/*           uniforme.                                                  */
/*                                                                       */
/* Sorties : la valeur tirée.                                             */
/*                                                                       */
/* Valeur de retour : aucun.                                            */
/* -------------------------------------------------------------------- */
void uniform(double a, double b)
{
    double r, res;

    r = genrand_real1();
    res = a + (b - a) * r;
    printf("Nombre aleatoire entre %f et %f: %.1f\n", a, b, res);
}
